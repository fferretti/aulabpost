<div class="container-fluid text-center">
    <div class="row vh-100 justify-content-center align-items-center">
        <div class="col-12">
            <h1 class="display-1">Abbiamo ricevuto una richiesta</h1>
            <h4>Richiesta per il ruolo di {{$info['role']}}</h4>
            <p>Ricevuta da {{$info['email']}}</p>
            <h4>Messaggio:</h4>
            <p>{{$info['message']}}</p>
        </div>
    </div>
</div>