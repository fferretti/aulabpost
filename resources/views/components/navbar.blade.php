<nav class="navbar navbar-expand-lg bg-navbar fixed-top">
    
    <div class="container-fluid">
        <i class="fa-solid fa-flag-checkered me-3"></i>
        <a class="navbar-brand mod nav-link" href="{{ route('welcome') }}">The aulab post</a>
        
        
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav w-100">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('welcome') }}">Home</a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="{{route('welcome')}}">
                        Benvenuto: {{ Auth::user()->name }}
                    </a>
                </li>
                @if(Auth::user()->is_writer)
                <li class="nav-item"><a class="nav-link" href="{{route('article.create')}}">Inserisci un articolo</a>
                </li>
                @endif

                <li class="nav-item"><a class="nav-link" href="{{route('careers')}}">Lavora con noi</a>
                </li>

                @if(Auth::user()->is_admin)
                <li class="nav-item"><a class="nav-link" href="{{route('admin.dashboard')}}">Admin</a>
                </li>
                @endif

                @if(Auth::user()->is_revisor)
                <li class="nav-item"><a class="nav-link" href="{{route('revisor.dashboard')}}">Revisor</a>
                </li>
                @endif

                @if(Auth::user()->is_writer)
                <li class="nav-item"><a class="nav-link" href="{{route('writer.dashboard')}}">Writer</a>
                </li>
                @endif

                <li class="ms-auto mx-2">
                <form class="d-flex" role="search" method="get" action="{{ route('article.search') }}">
                <input class="form-control me-2" type="form-control me-2" type="search" name="query" placeholder="Cosa stai cercando?" aria-label="Search">
                <button type="submit" class="btn btn-outline-success" type="submit">Cerca</button>
                </form>
                </li>

                <li class="">
                <form method="post" action="{{ route('logout') }}" id="form-logout">
                    @csrf
                <button type="submit" class="btn btn-danger">Logout</button>
                </form>
                </li>
                @endauth


                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                </li>
                @endguest


            </ul>
        </div>
    </div>
</nav>