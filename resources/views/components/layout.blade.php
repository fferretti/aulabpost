<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aulab Post</title>
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Mukta:wght@600&family=Unbounded:wght@600&display=swap" rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
    <x-navbar> 
    </x-navbar>
    <div class="min-vh-100">
        
        {{$slot}}

    </div>
   
    
</body>
<x-footer></x-footer>
</html>

<script src="https://kit.fontawesome.com/6206fa89bc.js" crossorigin="anonymous"></script>