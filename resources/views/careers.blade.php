<x-layout>
    <div class="container-fluid text-center pt-5 mt-5">
        <div class="row justify-content-center">
            <h1 class="display-1">
                Lavora con noi 
            </h1>
        </div>
    </div>

    <div class="container my-5">
        <div class="row justify-content-center align-items-center border rounded p-2 shadow">
            <div class="col-12 col-md-6">
                <h2>Lavora come amministratore</h2>
                <p>cosa farai: Lorem, ipsum dolor sit amet consectetur adipisicing elit. Amet, debitis?</p>
                <h2>lavora come revisore</h2>
                <p>cosa farai: Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus, rem?</p>
                <h2>lavora come redattore</h2>
                <p>cosa farai: Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus, saepe.</p>
            </div>
            <div class="col-12 col-md-6">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="p-5" action="{{ route('careers.submit') }}" method="post">
                    @csrf

                    <div class="mb-3">
                        <label for="role" class="form-label">per quale ruolo ti stai candidando?</label>
                        <select name="role" id="role" class="form-control">
                            <option value="">scegli qui</option>
                            <option value="admin">amministratore</option>
                            <option value="revisor">revisore</option>
                            <option value="writer">redattore</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">email</label>
                        <input name="email" type="email" class="form-control" id="email" value="{{old('email') ?? Auth::user()->email}}">
                    </div>
                    <div class="mb-3">
                        <label for="message" class="form-label">parlaci di te</label>
                        <textarea name="message" id="message" cols="30" rows="7" class="form-control">{{old('message')}}</textarea>
                    </div>
                    <div class="mt-2">
                        <button class="btn bg-info">invia la candidatura</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>
