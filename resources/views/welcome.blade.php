<x-layout>
    <div class="container-fluid background text-center">
        <div class="row justify-content-center align-items-center">
            @if (session('message'))
                <div class="alert alert-succes text-center">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>

            <header class="container-fluid mt-5">
                <div class="row justify-content-center bg-header mt-5">
                    <h1 class="titles mt-2 d-flex justify-content-center mt-5">The Aulab post</h1>
            </header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-5 mt-5 d-flex flex-column justify-content-center ms-3 hcol">
                        <h2>Il miglior sito per veri intenditori</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id impedit quis optio at laborum voluptas omnis magni, eligendi quo deserunt dolore necessitatibus officiis ipsa ea! Quam neque fugit error explicabo.</p>

                    </div>
                    <div class="col-12 col-md-6 d-flex justify-content-center align-items-center">
                        <img src="media/st.png" class="razzo" alt="razzo">
                </div>
            </div>
            </div>
            <div class="container-fluid">
            
                <div class="row justify-content-center mt-5">
                    <h2 class="d-flex justify-content-center annuncio">
                        Ultimi Annunci</h2>


          

            @foreach ($articles as $article)
                <div class="col-12 col-md-2">
                    
              
                    <div class="card">

                        <img src="{{Storage::url($article->image)}}" class="cstm-card-img-top img-fluid" alt="...">
                        <div class="card-body cstm-card-body">
                            <h5 class="card-title">{{ $article->title }}</h5>
                            <p class="card-text">{{ $article->subtitle }}</p>
                            
                            @if($article->category)
                            <a href="{{route('article.byCategory', ['category' => $article->category->id])}}" class="card-text">{{ $article->category->name }}</a>
                            @else
                            <p class="card-text">Non categorizzato</p>
                            @endif
                            <span class="text-muted small fst-italic">- tempo di lettura {{$article->readDuration()}} min</span>
                            

                            <p class="mt-4 small fst-italic text-capitalize">
                                @foreach($article->tags as $tag)
                                #{{$tag->name}}
                                @endforeach
                            </p>
                        </div>
                        <div class="card-footer bg-navbar">
                            Redatto il {{$article->created_at->format('d/m/Y')}} da <a href="{{route('article.byUser', ['user' => $article->user->id])}}">{{ $article->user->name }}</a>
                            <a href="{{route('article.show', compact('article'))}}" class="btn btn-primary">Leggi</a>
                        </div>
                    </div>

                </div>
               
            @endforeach
                </div>


                
       </div>
       
       
    
    <script src="https://kit.fontawesome.com/6206fa89bc.js" crossorigin="anonymous"></script>
</x-layout>
