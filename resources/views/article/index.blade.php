<x-layout>
    <div class="container-fluid text-center">
        <div class="row vh-100 justify-content-center align-items-center">
           
            <div class="col-12">
                <h1 class="display-1">Tutti gli articoli</h1>
            </div>

            @foreach ($articles as $article)
                <div class="col-12 col-md-3">

                    <div class="card">
                        <img src="{{ Storage::url($article->image) }}" class="card-img-top" alt="...">
                        <div class="card-body ">
                            <h5 class="card-title">{{ $article->title }}</h5>
                            <p class="card-text">{{ $article->subtitle }}</p>

                            @if($article->category)
                            <a href="{{route('article.byCategory', ['category' => $article->category->id])}}" class="card-text">{{ $article->category->name }}</a>
                            @else
                            <p class="card-text">Non categorizzato</p>
                            @endif
                            <span class="text-muted small fst-italic">- tempo di lettura {{$article->readDuration()}} min</span>

                        </div>
                        <div class="card-footer">
                            Redatto il {{$article->created_at->format('d/m/Y')}} da {{ $article->user->name }}
                            <a href="{{route('article.show', compact('article'))}}" class="btn btn-primary">Leggi</a>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    </div>
</x-layout>
