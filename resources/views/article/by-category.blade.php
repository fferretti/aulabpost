<x-layout>

    <div class="container-fluid text-center">
        <div class="row vh-100 justify-content-center align-items-center">
            <div class="col-12">
                <h1 class="display-1">Categoria {{ $category->name }}</h1>
            </div>

            @foreach ($articles as $article)
                <div class="col-12 col-md-3 ">

                    <div class="card bg-dark">
                        <img src="{{ Storage::url($article->image) }}" class="cstm-card-img-top" alt="...">
                        <div class="card-body cstm-card-body">
                            <h5 class="card-title">{{ $article->title }}</h5>
                            <p class="card-text">{{ $article->subtitle }}</p>
                            
                            <a href="{{route('article.byCategory', ['category' => $article->category->id])}}" class="card-text">{{ $article->category->name }}</a>
                        </div>
                        <div class="card-footer">
                            Redatto il {{$article->created_at->format('d/m/Y')}} da <a href="{{route('article.byUser', ['user' => $article->user->id])}}">{{ $article->user->name }}</a>
                            <a href="{{route('article.show', compact('article'))}}" class="btn btn-primary">Leggi</a>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    </div>
</x-layout>
