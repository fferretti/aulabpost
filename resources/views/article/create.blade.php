<x-layout>
    <div class="container-fluid text-center mt-5">
        <div class="row vh-100 justify-content-center align-items-center">
            <div class="col-12 mt-5">
                <h1 class="display-1">Inserisci un articolo</h1>
            </div>

            <div class="col-12 p-5">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="" action="{{ route('article.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="title" class="form-label">Titolo</label>
                        <input name="title" type="text" class="form-control" value="{{ old('title') }}" aria-describedby="titleHelp">
                    </div>
                    <div class="mb-3">
                        <label for="subtitle" class="form-label">Sottotitolo</label>
                        <input name="subtitle" type="text" class="form-control" value="{{ old('subtitle') }}" aria-describedby="subtitleHelp">
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Immagine</label>
                        <input name="image" type="file" class="form-control" value="{{ old('image') }}" aria-describedby="imageHelp">
                    </div>
                    <div class="mb-3">
                        <label for="category" class="form-label">Categoria</label>
                        <select name="category" class="form-control">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="tags" class="form-label">Tags:</label>
                        <input name="tags" id="tags" class="form-control" value="{{old('tags')}}">
                        <span class="small fst-italic">Dividi ogni tag con una virgola</span>
                    </div>

                    <div class="mb-3">
                        <label for="body">Corpo del testo</label>
                        <textarea name="body" id="body" cols="30" rows="10" class="form-control">{{old('body')}}</textarea>
                    </div>
                    <div class="d-flex">
                        <button class="btn btn-primary">Inserisci articolo</button>
                        <a href="{{ route('welcome') }}"><button class="btn btn-warning">Torna alla home</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>