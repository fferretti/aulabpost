<x-layout>
    <div class="container-fluid text-center">
        <div class="row vh-100 justify-content-center align-items-center">
            <div class="col-12 mt-5">
                <h1 class="display-1">Modifica un articolo</h1>
            </div>

            <div class="col-12 p-5">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{route('article.update', compact('article'))}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                    <div class="mb-3">
                        <label for="title" class="form-label">Titolo</label>
                        <input name="title" type="text" class="form-control" value="{{$article->title}}" aria-describedby="titleHelp">
                    </div>
                    <div class="mb-3">
                        <label for="subtitle" class="form-label">Sottotitolo</label>
                        <input name="subtitle" type="text" class="form-control" value="{{$article->subtitle}}" aria-describedby="subtitleHelp">
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Immagine:</label>
                        <input name="image" type="file" class="form-control" id="image" value="{{ old('image') }}">
                    </div>
                    <div class="mb-3">
                        <label for="category" class="form-label">Categoria:</label>
                        <select name="category" class="form-control">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($article->category && $category->id == $article->category->id) selected @endif>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="tags" class="form-label">Tags:</label>
                        <input name="tags" id="tags" class="form-control" value="{{old('tags')}}">
                        <span class="small fst-italic">Dividi ogni tag con una virgola</span>
                    </div>

                    <div class="mb-3">
                        <label for="body">Corpo del testo</label>
                        <textarea name="body" id="body" cols="30" rows="10" class="form-control">{{old('body')}}</textarea>
                    </div>
                    <div class="d-flex">
                        
                        <button type="submit" class="btn btn-primary">Modifica l'articolo</button>

                    </div>
                </form>
                <a href="{{ route('welcome') }}"><button class="btn btn-warning">Torna alla home</button></a>
            </div>
        </div>
    </div>
</x-layout>