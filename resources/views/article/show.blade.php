<x-layout>
    <div class="container-fluid text-center">
        <div class="row vh-100 justify-content-center align-items-center">
           
            <div class="col-12">
                <h1 class="display-1">{{$article->title}}</h1>
            </div>

            
                <div class="col-12">

                    <div class="">
                        <img class="cstm-card-img-top" src="{{ Storage::url($article->image) }}" class="card-img-top" alt="...">
                        <div class="text-center">
                            <p class="card-text">{{ $article->subtitle }}</p>
                            <p> Redatto da {{ $article->user->name }} il {{$article->created_at->format('d/m/Y')}}</p>                            
                        </div>
                        <hr>
                        <p>{{$article->body}}</p>
                        <meta name="keywords" class="mt-4 small fst-italic text-capitalize" content="@foreach($article->tags as $tag){{$tag->name}}, @endforeach">
                            @foreach($article->tags as $tag)
                            #{{$tag->name}}
                            @endforeach
                        </meta>
                        <div class="card-footer">
                            
                            <a href="{{route('article.index')}}" class="btn btn-primary">Torna indietro</a>
                            @if (Auth::user() && Auth::user()->is_revisor)
                            <a href="{{route('revisor.acceptArticle', compact('article'))}}" class="btn btn-success text-white my-5">Accetta articolo</a>
                            <a href="{{route('revisor.rejectArticle', compact('article'))}}" class="btn btn-danger text-white my-5">Rifiuta articolo</a>
                                
                            @endif

                           
                        </div>
                    </div>

                </div>
       

        </div>
    </div>
</x-layout>
